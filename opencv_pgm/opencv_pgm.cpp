// opencv_pgm.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "opencv2/opencv.hpp"
#include "opencv2/imgproc.hpp"
#include <algorithm>

cv::Mat frame;
cv::Scalar red(0, 0, 255);
cv::Scalar green(0, 255, 0);
cv::Scalar blue(255, 0, 0);

auto patterns_flat = std::vector<cv::Point2f>{
	{ 0.,0. } ,{ 1.,0. } ,{ 2.,0. } ,{ 3.,0., } ,{ 4.,0. } ,{ 5.,0. } ,
	{ 0.,1. } ,{ 1.,1. } ,{ 2.,1. } ,{ 3.,1., } ,{ 4.,1. } ,{ 5.,1. } ,
	{ 0.,2. } ,{ 1.,2. } ,{ 2.,2. } ,{ 3.,2., } ,{ 4.,2. } ,{ 5.,2. } ,
	{ 0.,3. } ,{ 1.,3. } ,{ 2.,3. } ,{ 3.,3., } ,{ 4.,3. } ,{ 5.,3. } , 
};
auto patterns = std::vector<std::vector<cv::Point2f>>{ {} };

void onMouse(int event, int x, int y, int, void* userdata)
{
	static std::vector<cv::Point2f> points;

	if (event == cv::EVENT_LBUTTONUP)
	{
		cv::Mat display;
		frame.copyTo(display);

		points.emplace_back(x, y);

		for (auto point : points)
		{
			cv::drawMarker(display, point, blue);
		}

		if (points.size() == 2)
		{
			cv::line(display, points[0], points[1], green);

			auto H = cv::findHomography(patterns[0], patterns_flat, cv::RANSAC);
			std::vector<cv::Point2f> points_transformed;
			cv::perspectiveTransform(points, points_transformed, H);
			auto points_distance = cv::norm(points_transformed[0] - points_transformed[1]);
			cv::putText(display, 
				std::to_string(points_distance),
				{ 15, 15 }, 
				cv::FONT_HERSHEY_COMPLEX_SMALL, 1.0, { 255, 255, 255 }, 1);
			cv::putText(display,
				"{ " + std::to_string(points_transformed[0].x) + " ; " + std::to_string(points_transformed[0].y) + "}",
				{ 15, 30 },
				cv::FONT_HERSHEY_COMPLEX_SMALL, 1.0, { 255, 255, 255 }, 1);
			cv::putText(display,
				"{ " + std::to_string(points_transformed[1].x) + " ; " + std::to_string(points_transformed[1].y) + "}",
				{ 15, 45 },
				cv::FONT_HERSHEY_COMPLEX_SMALL, 1.0, { 255, 255, 255 }, 1);

			points.clear();
		}

		imshow("source", display);
	}
}

int main()
{
	cv::VideoCapture cap(0);
	if (!cap.isOpened())
		return -1;

	cv::Mat gray, cube, otsu;
	cv::Mat _intrinsicsMatrix, _distortionCoeffs, _rvecs, _tvecs;
	std::vector<cv::Point2f> corners;
	cv::Size board_sz(6, 4);
	auto chessboard_mesh = std::vector<std::vector<cv::Point3f>>{ {
		{ 0.,0.,0. } ,{ 1.,0.,0. } ,{ 2.,0.,0. } ,{ 3.,0.,0. } ,{ 4.,0.,0. } ,{ 5.,0.,0. } ,
		{ 0.,1.,0. } ,{ 1.,1.,0. } ,{ 2.,1.,0. } ,{ 3.,1.,0. } ,{ 4.,1.,0. } ,{ 5.,1.,0. } ,
		{ 0.,2.,0. } ,{ 1.,2.,0. } ,{ 2.,2.,0. } ,{ 3.,2.,0. } ,{ 4.,2.,0. } ,{ 5.,2.,0. } ,
		{ 0.,3.,0. } ,{ 1.,3.,0. } ,{ 2.,3.,0. } ,{ 3.,3.,0. } ,{ 4.,3.,0. } ,{ 5.,3.,0. } ,
	} };
	auto cube_corners_3d = std::vector<cv::Point3f>{ 
		{ 0.,0.,0. } ,{ 0.,3.,0. } ,{ 3.,3.,0. } ,{ 3.,0.,0. } , 
		{ 0.,0.,-3. } ,{ 0.,3.,-3. } ,{ 3.,3.,-3. } ,{ 3.,0.,-3. } ,
	};
	auto cube_corners_2d = std::vector<cv::Point2f>{};
	auto zero_distortion = cv::Mat::zeros(8, 1, CV_64F);

	std::vector<std::vector<cv::Point>> contours;

	for (;;)
	{
		cap >> frame;
		imshow("source", frame);

		cvtColor(frame, gray, cv::COLOR_BGR2GRAY);
		imshow("gray", gray);

		auto found = cv::findChessboardCorners(gray, board_sz, corners, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);
		if (!found) continue;
		patterns[0] = corners;

		frame.copyTo(cube);
		cv::drawChessboardCorners(cube, board_sz, corners, true);
		cv::imshow("Chessboard", cube);

		cv::calibrateCamera(chessboard_mesh, patterns, cube.size(), _intrinsicsMatrix, _distortionCoeffs, _rvecs, _tvecs);

		cv::projectPoints(cube_corners_3d, _rvecs, _tvecs, _intrinsicsMatrix, _distortionCoeffs, cube_corners_2d);
		
		cv::line(cube, cube_corners_2d[0], cube_corners_2d[1], green);
		cv::line(cube, cube_corners_2d[1], cube_corners_2d[2], green);
		cv::line(cube, cube_corners_2d[2], cube_corners_2d[3], green);
		cv::line(cube, cube_corners_2d[3], cube_corners_2d[0], green);
															   
		cv::line(cube, cube_corners_2d[4], cube_corners_2d[5], green);
		cv::line(cube, cube_corners_2d[5], cube_corners_2d[6], green);
		cv::line(cube, cube_corners_2d[6], cube_corners_2d[7], green);
		cv::line(cube, cube_corners_2d[7], cube_corners_2d[4], green);
															   
		cv::line(cube, cube_corners_2d[0], cube_corners_2d[4], green);
		cv::line(cube, cube_corners_2d[1], cube_corners_2d[5], green);
		cv::line(cube, cube_corners_2d[2], cube_corners_2d[6], green);
		cv::line(cube, cube_corners_2d[3], cube_corners_2d[7], green);
		
		cv::imshow("cube", cube);

		cv::Mat ranged;
		cv::inRange(frame, cv::Scalar(40, 40, 180), cv::Scalar(90, 90, 240), ranged);
		cv::imshow("ranged", ranged);

		cv::threshold(gray, otsu, 100, 255, cv::THRESH_OTSU);
		cv::imshow("otsu", otsu);

		cv::findContours(otsu, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
		
		auto max_contour = std::max_element(contours.cbegin(), contours.cend(),
			[](const std::vector<cv::Point>& c1, const std::vector<cv::Point>& c2) {
			return contourArea(c1) < contourArea(c2);
		});
		cv::Mat max_frame(frame);
		if (max_contour != contours.cend())
		{
			cv::polylines(max_frame, *max_contour, true, green, 3);
			cv::imshow("max", max_frame);
		}

		cv::setMouseCallback("source", onMouse);

		if (cv::waitKey() == 27)
			break;
	}
    return 0;
}

